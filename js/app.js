(function() {

    'use strict';

    var db = null;
    var dbname = 'idb://pouch_intro';


	var messages = [];
	

    window.addEventListener( 'load', loadPouch, false );

    function loadPouch() {
        Pouch(dbname, function(err, pouchdb){
            if(err){
                alert("Can't open pouchdb database");
            }else{
                db = pouchdb;
                windowLoadHandler();
            }
        });
    };

    function windowLoadHandler() {
        //Other logic to be executed when the page loads should be placed here
        addEventListeners();
        
        loadPage();
        
        
        console.log(db);
    };

    function addEventListeners() {
        //Hook in to various parts of the page
        document.getElementById('upload').addEventListener( 'click', addToDB, false);
        //document.getElementById('show').addEventListener( 'click', showText, false);
        //document.getElementById('reset').addEventListener( 'click', reset, false);
        
        $('.reset-link').click(function(){ reset(); });

		
		

    };

    var reset = function(){
    
        Pouch.destroy(dbname, function(err1){
    
            if(err1){
                alert("Database destruction error")
            } else {
                Pouch(dbname, function(err2, pouchdb){
                    if(err2){
                        alert("Database creation error")
                    } else {
                        db= pouchdb;
                    } })
            }
        });
        
        loadPage();
    };



    var addToDB = function(){

		var currentDateObject = new Date(); 
		var dateString = currentDateObject.getFullYear() + '-' + (currentDateObject.getMonth()+1) + '-' + currentDateObject.getDate();
    
        var text = document.getElementById('enter-text').value;
        text = $.trim(text);
        
        if(text != '') {
	        db.post({
	        	parent: 0,
	        	text: text,
	        	date: dateString,
	        	status: 1
	        });
	        loadPage();
    	} else {
    		addMSG('Text empty');
    	}
     	
     
//     	 var changes = db.changes({
//     	   continuous: true,
//     	   onChange: function(change){
//     			loadPage();
//     	   }
//     	 });
//     	 changes.cancel();
//     	
     	
    };


    var loadPage = function(){
        
        db.allDocs({include_docs: true}, function(err, result){
            
            if(!err){
            
                var out= "";
                
                result.rows.reverse();
                
                result.rows.forEach(function(element){
                    out+= '<li class="panel item" data-date="' + element.doc.date + '" data-status="' + element.doc.status + '" data-parent="' + element.doc.parent + '"><span class="date">' + element.doc.date + '</span> ' + element.doc.text + ' <span class="options"><a href="">Delete</a></span></li>';
                });
                $('#tasks-list').html(out);
            }
        })
        	    
	    db.allDocs({include_docs: true}, function(err, result){
	        
	        if(!err){
	        
	            var out = '<optgroup label="Select Parent">';
	            
	            result.rows.reverse();
	            
	            result.rows.forEach(function(element){
	                out += '<option value="'+ element.doc.id +'">' + element.doc.text + '</option>';
	            });
	            
	            out += '</optgroup>';
	            
	            
	            $('#parent-selection').html(out);
	        }
	    })

	};




    var showTextAlternative = function(){
    
        var map = function(doc){
            if(doc.text){
                emit(doc._id, doc.text);
            }
        };

        db.query({map: map}, function(err, res){
            if(!err){
                var out= "";
                res.rows.forEach(function(element){
                    out+= element.value + '<br>';
                });
                document.getElementById('tasks-list').innerHTML= out;
            }
        })
    };
    
    
    var addMSG = function(text, type) {
    	messages.push(text);
    	if(type==NaN){
    		var type = 'default';
    	}
    	$('.add-form').before('<div class="panel message message-'+type+'"><ul>'+text+'</ul></div>');
    };
    
        
})();
